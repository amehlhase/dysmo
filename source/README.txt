Start here with your structural dynamics simulation experience.

==============================
To run the delivered examples:

1. Install the delivered python version and all packages

2. Set environment variables (refer to Install-Guide in Documentation)

3. Set all paths and desiered tools in "userSettings.py"

4. Build your solution with "python setup.py build" in your command window

5. Set the model you like to simulate in "scripts/testbench.py"

6. Set the model parameters and transitions in "scripts/<modelName>.py"

7. Run the simulation with "python testbench.py"


To run the examples run the files:
pendulum.py
pendulumDS.py
bouncingBallSame.py
bounceWall.py
advancedBallDO.py
bouncingballD.py

from the source/scripts folder

